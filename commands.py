commands_description = {
    "add-admin": "add admin to the system",
    "add-user": "add user to the system",
    "help": "returns the list of available commands with a short description",
    "inbox": "open inbox with all received messages",
    "info": "returns the server version number and date of creation",
    "login": "opens the system login form",
    "send": "send a message to another user",
    "send-to-all": "send message to all users",
    "stop": "stop server and client simultaneously",
    "uptime": "returns the uptime of the server",
}
